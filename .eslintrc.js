module.exports = {
  env: {
    browser: true,
    es2020: true,
    node: true,
  },
  settings: {
    // 'import/resolver': {
    //   typescript: {},
    // },
    files: ['.ts', '.tsx'],
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
    },
  },
  ignorePatterns: ['.eslintrc.js', '*.config.js'],
  extends: [
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'airbnb',
    'airbnb-typescript',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['tsconfig.json'],
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'react/jsx-filename-extension': ['warn', { extensions: ['.tsx'] }],
    'jsx-a11y/anchor-is-valid': "off",
    'react/prop-types': "off",
    'react/prefer-stateless-function': "off",
    'react/destructuring-assignment': 'off',
    "indent": ["error", 2],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        ts: 'never',
        tsx: 'never',
      },
    ],
  },
};
