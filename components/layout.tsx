import React from 'react';
import Header from './header';

export default function Layout({ children }): React.ReactElement {
  return (
    <>
      <Header />
      <main>
        {children}
      </main>
    </>
  );
}
